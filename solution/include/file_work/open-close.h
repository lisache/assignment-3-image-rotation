#pragma once
#include "../include/file_work/file_work_const.h"
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>


file_work_error_code_t check_args(int argc, char** argv);
FILE* open_file(const char* filename, uint8_t mode);
void perform_file_working(file_work_error_code_t status);
