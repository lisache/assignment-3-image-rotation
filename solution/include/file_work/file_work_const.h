#pragma once
typedef enum {
	OK = 0,
	READ_INVALID_COUNT_ARGS,
	READ_NULL_FILE,
	READ_INVALID_FILE_ARGUMENTS,
	FILE_SUCCESSFULLY_OPENED,
	FILE_CLOSE_ERROR,
	/* more codes */
} file_work_error_code_t;


static const char* const fw_error_messages[] = {
[OK] = "Successful",
[READ_INVALID_COUNT_ARGS] = "Invalid count of arguments\n",
[READ_INVALID_FILE_ARGUMENTS] = "Invalid file arguments\n",
[READ_NULL_FILE] = "Error while reading. Cannot open\n",
[FILE_SUCCESSFULLY_OPENED] = "File was sucsessfully opened\n",
[FILE_CLOSE_ERROR] = "Cannot close file\n",
};
