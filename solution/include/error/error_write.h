#pragma once
typedef enum {
	WRITE_OK = 0,
	WRITE_ERROR,
	WRITE_INVALID_FILE,
	WRITE_HEADER_ERROR,
	WRITE_IMAGE_ERROR,
	WRITE_PADDING_ERROR
	/* more codes */
} write_error_code_t;

static const char* const wf_error_messages[] = {
[WRITE_OK] = "BMP file was successfully written",
[WRITE_ERROR] = "Error while writing file.",
[WRITE_INVALID_FILE] = "Unable to write to file",
[WRITE_HEADER_ERROR] = "Error while writing header",
[WRITE_IMAGE_ERROR] = "Error while writing pixel array",
[WRITE_PADDING_ERROR] = "Error while writing padding"
};
