#pragma once

#ifndef _ERROR_READ
#define _ERROR_READ

#include <stdio.h>

typedef enum {
	READ_OK = 0,
	READ_INVALID_SIGNATURE,
	READ_INVALID_BITS,
	READ_INVALID_HEADER,
	READ_INVALID_FILE_ARGS,
	READ_EMPTY_IMAGE,
	READ_INVALID_IMAGE_POINTER,
	READ_IMAGE_FAIL
 /* more codes */
 } read_error_code_t;


static const char* const rf_error_messages[] = {
[READ_OK] = "BMP file read successfully",
[READ_INVALID_SIGNATURE] = "Error while reading. Invalid signature",
[READ_INVALID_BITS] = "Error while loading image. Invalid count of bits",
[READ_INVALID_HEADER] = "Error while reading header",
[READ_INVALID_FILE_ARGS] = "Error while reading input file args",
[READ_EMPTY_IMAGE] = "Error while reading. Image has zero size",
[READ_INVALID_IMAGE_POINTER] = "Error while loading image",
[READ_IMAGE_FAIL] = "Unable to read image pixels"
};
#endif // !_ERROR_READ

