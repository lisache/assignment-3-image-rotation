#ifndef _ROTATE
#define _ROTATE

#include "../image/image_format.h"
#include <malloc.h>

struct image_t rotate_to_90_degrees(struct image_t img);
#endif

