#pragma once

#include "../error/error_write.h"
#include "../image/image_format.h"

write_error_code_t to_bmp(FILE* out, struct image_t const* img);
void perform_writing_file(write_error_code_t status);
