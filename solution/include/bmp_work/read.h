#pragma once

#ifndef _READ
#define _READ

#include "../error/error_read.h"
#include "../image/image_format.h"
#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>


read_error_code_t from_bmp(FILE* in, struct image_t* const read);
void perform_reading_file(read_error_code_t status);


#endif





