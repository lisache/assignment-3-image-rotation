#ifndef _IMAGE_FORMAT
#define _IMAGE_FORMAT
#include <stdint.h>


#pragma pack(push, 1)

struct pixel_t { char b, g, r; };
#pragma pack(pop)

#pragma pack(push, 1)
struct image_t {
	uint32_t width, height;
	struct pixel_t* data;
};
#pragma pack(pop)
#endif // !_IMAGE_FORMAT





