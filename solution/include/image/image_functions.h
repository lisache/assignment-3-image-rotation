#ifndef _IMAGE_FUNCTIONS
#define _IMAGE_FUNCTIONS
#include "image_format.h"
#include <malloc.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

struct image_t create_new_image(const uint32_t height, const uint32_t width);
bool check_creation(struct image_t* const img);
void image_delete(const struct image_t*const image);
struct pixel_t get_pixel(const struct image_t img, const uint32_t row, const uint32_t column);
uint32_t find_padding(uint32_t width);

#endif // !_IMAGE_FUNCTIONS
