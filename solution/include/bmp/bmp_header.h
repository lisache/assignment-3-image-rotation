#ifndef _BMP_HEADER
#define _BMP_HEADER

#define FILE_TYPE 19778
#define BIT_COUNT 24   //bit per pixel
#define RESERVED 0
#define OFFSET sizeof(bmp_header_t)
#define BI_SIZE 40     //size of BITMAPINFO (version 3)
#define PLANES 1       // in BPM always one
#define COMPRESSION 0
#define XPPM 2834     
#define YPPM 2834
#define COLOR_USED 0
#define COLOR_IMPORTANT 0

#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>


#pragma pack(push, 1)
typedef struct 
{
    uint16_t fileType;
    uint32_t fileSize;
    uint32_t fileReserved;
    uint32_t offsetBits;
    uint32_t biSize;

    uint32_t width;
    uint32_t height;
    uint16_t planes;
    uint16_t bitCount;
    uint32_t biCompression;
    uint32_t sizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
} bmp_header_t;
#pragma pack(pop)

#endif
