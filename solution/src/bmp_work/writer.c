#include "../include/bmp/bmp_header.h"
#include "../include/error/error_write.h"
#include "../include/image/image_format.h"
#include "../include/image/image_functions.h"
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#define OK 0
#define COUNTER 1


//creating new bmp header with correct fields
bmp_header_t new_header_to_write(struct image_t const* img) {
	return (bmp_header_t) {
		.biClrImportant = COLOR_IMPORTANT,
		.biClrUsed = COLOR_USED,
		.biCompression = COMPRESSION,
		.biXPelsPerMeter = XPPM,
		.biYPelsPerMeter = YPPM,
		.fileReserved = RESERVED,
		.fileType = FILE_TYPE,
		.bitCount = BIT_COUNT,
		.biSize = BI_SIZE,
		.sizeImage = sizeof(struct pixel_t) * img->width * img->height,
		.height = img->height,
		.width = img->width,
		.planes = PLANES,
		.offsetBits = OFFSET,
		.fileSize = sizeof(bmp_header_t) + sizeof(struct pixel_t) * img->width * img->height
	};
}

//writing image to bmp file
write_error_code_t to_bmp(FILE* out, struct image_t const* img) {

	//check if pointer to output file is not null
	if (out == NULL) {
		return WRITE_INVALID_FILE;
	}
	//creating header
	bmp_header_t header = new_header_to_write(img);
	//write header to output file, if possible
	if (fwrite(&header, sizeof(bmp_header_t), 1, out) != COUNTER) {
		return WRITE_HEADER_ERROR;
	}
	const uint32_t h = img->height;
	const uint32_t w = img->width;
	//calculating padding for current width
	uint32_t pad = find_padding(w);
	uint32_t padding_value = 0;
	uint32_t i = 0;
	while (i < h) {
		//write pixels to file
		if (fwrite(img->data + i * w, sizeof(struct pixel_t) * w, COUNTER, out) != COUNTER) {
			return WRITE_IMAGE_ERROR;
		}
		//write padding to file if needed
		if (((fwrite(&padding_value, pad, 1, out)) != COUNTER) ) {
			return WRITE_PADDING_ERROR;
		}
		i++;
	}
	return WRITE_OK;
}

//function for printing results to stderr or stdout
void perform_writing_file(write_error_code_t status) {
	if (status == OK) {
		fprintf(stdout, "%s", wf_error_messages[status]);
	}
	else {
		fprintf(stderr, "%s", wf_error_messages[status]);
	}

}
