#include "../include/image/image_format.h"
#include "../include/image/image_functions.h"
#include <malloc.h>
#include <stdint.h>
#include <stdlib.h>


struct image_t rotate_to_90_degrees(const struct image_t img) {
	uint32_t const height = img.height;
	uint32_t const width = img.width;
    struct image_t new_img = create_new_image(width, height);


	for (uint32_t i = 0; i < height; i++)
	{
		for (uint32_t j = 0; j < width; j++)
		{
			struct pixel_t pixel_from_prev = get_pixel(img, i, j);
			new_img.data[(height - i - 1) + height * j] = pixel_from_prev;
		}
	}

	
	

	return new_img;


}
