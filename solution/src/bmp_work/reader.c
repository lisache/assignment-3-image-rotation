#include "../include/bmp/bmp_header.h"
#include "../include/error/error_read.h"
#include "../include/image/image_format.h"
#include "../include/image/image_functions.h"
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#define COUNT 1
#define OK 0





static bool read_pixel(FILE* in, const uint32_t width, const uint32_t height, struct pixel_t* const pixels, const uint32_t padding) {
    uint32_t i = 0;
    bool ok = true;
    while (i < height) {
        if ((fread(pixels + width * i, sizeof(struct pixel_t), width, in) != width) || 
            (fseek(in, (long) padding, SEEK_CUR) != OK)) {
            ok = false;
            break;
        }
        i = i + 1;
    }
    return ok;
}


//trying to read from bmp file
read_error_code_t from_bmp(FILE *in, struct image_t* const read) {

    //check if pointer to input file is not null
    if (!(in == NULL)) {
        bmp_header_t header = { 0 };

        //trying to read header
        if (fread(&header, sizeof(bmp_header_t), COUNT, in) == COUNT) {

            //checking if header fields are correct
            if (FILE_TYPE == header.fileType) {
                if (header.bitCount == BIT_COUNT) {

                    //creating new image with height and width read from header
                    const uint32_t width = header.width;
                    const uint32_t height = header.height;
                    *read = create_new_image(height, width);

                    //checking if new image creation was successful
                    if (check_creation(read) == true) {

                        //moves the file pointer to the starting position of the image
                        if (fseek(in, (long) header.offsetBits, SEEK_SET) == OK) {
                            //reading pixels
                            if ((read_pixel(in, width, height, read->data, find_padding(width)))) {
                                return READ_OK;
                            }
                            image_delete(read);
                            return READ_IMAGE_FAIL;
                        }
                        return READ_INVALID_IMAGE_POINTER;
                    }
                    image_delete(read);
                    return READ_EMPTY_IMAGE;
                }
                return READ_INVALID_BITS;
            }
            else {
                
                return READ_INVALID_SIGNATURE;
            }
            
        }
        else {
            return READ_INVALID_HEADER;
        }
    }
 
     return READ_INVALID_FILE_ARGS;
    

   
    
}


//function for printing results to stderr or stdout
void perform_reading_file(read_error_code_t status) {
    if (status == OK) {
        fprintf(stdout, "%s", rf_error_messages[status]);
    }
    else {
        fprintf(stderr, "%s", rf_error_messages[status]);
    }
    
}


