#include "../include/image/image_format.h"
#include <malloc.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>





struct image_t create_new_image(const  uint32_t height, const uint32_t width) {
	struct image_t img = {
	  .width = width,
	  .height = height,
	  .data = malloc(sizeof(struct pixel_t) * width * height) 
	};

	return img;

}

bool check_creation(struct image_t * const img) {
	if (img->data == NULL) {
		return false;
	}
	return true;
}

void image_delete(const struct image_t* const image)
{
	free(image->data);
}

struct pixel_t get_pixel(const struct image_t img, const uint32_t row, const uint32_t column)
{
	return img.data[row * img.width + column];
}

//function for calculating paddings (multiplicity of four)
 uint32_t find_padding(uint32_t width) {
	return (4 - (width * sizeof(struct pixel_t) % 4));
}
