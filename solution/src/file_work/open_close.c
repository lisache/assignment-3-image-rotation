#include "../include/file_work/file_work_const.h"
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

file_work_error_code_t check_args(int argc, char** argv) {
	if (argc != 3) {
		
		return READ_INVALID_COUNT_ARGS;
	}
	if (argv[1] == NULL || argv[2] == NULL) {
		return READ_INVALID_FILE_ARGUMENTS;
	}
	return OK;
}

FILE* open_file(const char* filename, uint8_t mode) {
	FILE* file = NULL;
	if (mode == 0) {
		file = fopen(filename, "rb");
	}
	if (mode == 1) {
		file = fopen(filename, "wb");
	}
	return file;
}

 
void perform_file_working(file_work_error_code_t status) {
	if (status == 0) {
		fprintf(stdout, "%s", fw_error_messages[status]);
	}
	else {
		fprintf(stderr, "%s", fw_error_messages[status]);
	}

}
