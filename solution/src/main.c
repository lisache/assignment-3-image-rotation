#include "../include/bmp_work/read.h"
#include "../include/bmp_work/rotate.h"
#include "../include/bmp_work/write.h"
#include "../include/error/error_write.h"
#include "../include/file_work/open-close.h"
#include "../include/image/image_format.h"
#include "../include/image/image_functions.h"
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#define READ 0
#define WRITE 1


int main( int argc, char** argv ) {
    //(void) argc; (void) argv; // supress 'unused parameters' warning
    
    //checking if arguments are correct
    file_work_error_code_t flag = check_args(argc, argv);
    if (flag != 0) {
        perform_file_working(flag);
        exit(1);
    }
    
    FILE* input_file = open_file(argv[1], READ);
    if (input_file == NULL)
    {
        flag = READ_NULL_FILE;
        perform_file_working(flag);
        exit(1);
    }
    
    perform_file_working(FILE_SUCCESSFULLY_OPENED);

    struct image_t image = { 0 };

    //trying to read from bmp file
    read_error_code_t status = from_bmp(input_file, &image);
    perform_reading_file(status);
    if (status != 0) {
        exit(1);
    }

    if (fclose(input_file) != 0) {
        perform_file_working(FILE_CLOSE_ERROR);
        exit(1);
    }

    //creating new rotated image
    struct image_t const new_image = rotate_to_90_degrees(image);
    image_delete(&image);
            
    //trying to open an output file 
    FILE* output_file = open_file(argv[2], WRITE);
    if (output_file == NULL)
    {
        perform_file_working(READ_NULL_FILE);
        exit(1);
    }
    perform_file_working(FILE_SUCCESSFULLY_OPENED);

    //trying to write rotated image to output file
    write_error_code_t wStatus = to_bmp(output_file, &new_image);
    perform_writing_file(wStatus);
    if (wStatus != 0) {
        exit(1);
    }
                

    image_delete(&new_image);
    perform_file_working(OK);
    exit(0);


}
